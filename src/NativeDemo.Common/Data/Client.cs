﻿namespace NativeDemo.Common.Contracts
{
    public sealed class Client
    {
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public string ContactInfoFormatted => $"Email: '{this.Email}', GSM: '{this.PhoneNumber}'";
    }
}
