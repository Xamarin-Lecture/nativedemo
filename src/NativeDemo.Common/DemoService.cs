﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NativeDemo.Common.Contracts;

namespace NativeDemo.Common
{
    public class DemoService
    {
        #region Constructor

        private IApiClient _apiClient;

        private DemoService()
        {
            //this._apiClient = new ApiClient();
            this._apiClient = new MockedApiClient();
        }

        #endregion Constructor

        #region Clients

        private List<Client> _cachedClientList;

        public async Task<List<Client>> GetClientList()
        {
            if (this._cachedClientList == null)
            {
                this._cachedClientList = await this.CallAsync(this._apiClient.GetClientList()) ?? new List<Client>();
            }

            return this._cachedClientList;
        }

        #endregion Clients

        #region Infrastructure

        private async Task<TData> CallAsync<TData>(Task<TData> apiCall)
             where TData : class
        {
            try
            {
                return await apiCall.ConfigureAwait(false);
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion Infrastructure

        #region Singleton

        public static DemoService Instance => DemoService._instance.Value;

        private static Lazy<DemoService> _instance = new Lazy<DemoService>(DemoService.Initializer);

        private static DemoService Initializer()
        {
            return new DemoService();
        }

        #endregion Singleton
    }
}
