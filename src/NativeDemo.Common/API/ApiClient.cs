﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ModernHttpClient;
using NativeDemo.Common.Contracts;

namespace NativeDemo.Common
{
    internal class ApiClient : IApiClient
    {
        private HttpClient _client;

        internal ApiClient()
        {
            var nativeHandler = new NativeMessageHandler
            {
                Timeout = TimeSpan.FromSeconds(Constants.TimeoutInSeconds)
            };

            this._client = new HttpClient(nativeHandler);
            this._client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.MediaTypeString));
        }

        public Task<List<Client>> GetClientList()
        {
            return this._client.GetAsync<List<Client>>("https://api.myjson.com/bins/79efw");
        }
    }
}
