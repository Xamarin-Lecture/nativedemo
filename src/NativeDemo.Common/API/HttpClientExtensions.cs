﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NativeDemo.Common
{
    internal static class HttpClientExtensions
    {
        public static async Task<T> PostAsync<T>(this HttpClient client, string path, object @object)
            where T : class
        {
            var serializedObject = JsonConvert.SerializeObject(@object);
            var content = new StringContent(serializedObject, Encoding.UTF8, Constants.MediaTypeString);
            var response = await client.PostAsync(path, content).ConfigureAwait(false);

            return await response.Compile<T>().ConfigureAwait(false);
        }

        public static async Task<T> GetAsync<T>(this HttpClient client, string path)
            where T : class
        {
            var response = await client.GetAsync(path).ConfigureAwait(false);

            return await response.Compile<T>().ConfigureAwait(false);
        }

        public static Task<byte[]> GetFileAsync(this HttpClient client, string path)
        {
            return client.GetByteArrayAsync(path);
        }

        public static async Task<T> Compile<T>(this HttpResponseMessage httpResponse)
            where T : class
        {
            if (!httpResponse.IsSuccessStatusCode)
            {
                return null;
            }

            var content = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
