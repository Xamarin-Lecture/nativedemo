﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NativeDemo.Common.Contracts;

namespace NativeDemo.Common
{
    internal class MockedApiClient : IApiClient
    {
        public Task<List<Client>> GetClientList()
        {
            var result = new List<Client>
            {
                new Client
                {
                    ClientId = 1,
                    Name = "Marko Marković",
                    Email = "marko.markovic@gmail.com",
                    PhoneNumber = "0911234567"
                },
                new Client
                {
                    ClientId = 2,
                    Name = "Ivan Horvat",
                    Email = "ivan.horvat@gmail.com",
                    PhoneNumber = "0981234567"
                },
            };

            return this.TaskFromResult(result);
        }

        private Task<TResult> TaskFromResult<TResult>(TResult result)
        {
            return Task
                .Delay(Constants.MockedApiClientDelayInMs)
                .ContinueWith((previousTask) => result);
        }
    }
}
