﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NativeDemo.Common.Contracts;

namespace NativeDemo.Common
{
    internal interface IApiClient
    {
        Task<List<Client>> GetClientList();
    }
}
