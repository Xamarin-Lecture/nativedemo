﻿namespace NativeDemo.Common
{
    internal class Constants
    {
        public const int TimeoutInSeconds = 60;
        public const string MediaTypeString = "application/json";
        public const int MockedApiClientDelayInMs = 250;
    }
}
