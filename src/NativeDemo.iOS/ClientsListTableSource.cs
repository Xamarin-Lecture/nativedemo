﻿using System;
using System.Collections.Generic;

using Foundation;
using NativeDemo.Common.Contracts;
using UIKit;

namespace Blank
{
    public class ClientsListTableSource : UITableViewSource
    {
        private List<Client> _list;

        public ClientsListTableSource(List<Client> list)
        {
            this._list = list;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(nameof(ClientRow)) as ClientRow;
            var data = this._list[indexPath.Row];

            cell.UpdateRow(data);

            return cell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return UITableView.AutomaticDimension;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return this._list.Count;
        }
    }
}