﻿using NativeDemo.Common;
using UIKit;

namespace Blank
{
    public class ListViewController : UIViewController
    {
        private UITableView _tableView;

        public async override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this._tableView = new UITableView()
            {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            this._tableView.RegisterClassForCellReuse(typeof(ClientRow), nameof(ClientRow));

            this.View.AddSubview(this._tableView);

            this._tableView.TopAnchor.ConstraintEqualTo(this.View.SafeAreaLayoutGuide.TopAnchor).Active = true;
            this._tableView.LeftAnchor.ConstraintEqualTo(this.View.SafeAreaLayoutGuide.LeftAnchor).Active = true;
            this._tableView.RightAnchor.ConstraintEqualTo(this.View.SafeAreaLayoutGuide.RightAnchor).Active = true;
            this._tableView.BottomAnchor.ConstraintEqualTo(this.View.SafeAreaLayoutGuide.BottomAnchor).Active = true;

            var clients = await DemoService.Instance.GetClientList();
            this._tableView.Source = new ClientsListTableSource(clients);
            this._tableView.ReloadData();
        }
    }
}