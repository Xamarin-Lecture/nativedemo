﻿using System;
using NativeDemo.Common.Contracts;
using UIKit;

namespace Blank
{
    public class ClientRow : UITableViewCell
    {
        private UILabel _label1, _label2;

        public ClientRow(IntPtr handle) : base(handle)
        {
            this._label1 = new UILabel()
            {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            this._label2 = new UILabel()
            {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            this.AddSubviews(this._label1, this._label2);

            this._label1.TopAnchor.ConstraintEqualTo(this.TopAnchor, 10).Active = true;
            this._label1.LeftAnchor.ConstraintEqualTo(this.LeftAnchor, 10).Active = true;
            this._label1.RightAnchor.ConstraintEqualTo(this.RightAnchor).Active = true;

            this._label2.TopAnchor.ConstraintEqualTo(this._label1.TopAnchor, 20).Active = true;
            this._label2.LeftAnchor.ConstraintEqualTo(this.LeftAnchor, 10).Active = true;
            this._label2.RightAnchor.ConstraintEqualTo(this.RightAnchor).Active = true;
            this._label2.BottomAnchor.ConstraintEqualTo(this.BottomAnchor, 10).Active = true;

            this._label1.SizeToFit();
            this._label2.SizeToFit();
        }

        public void UpdateRow(Client data)
        {
            this._label1.Text = data.Name;
            this._label2.Text = data.ContactInfoFormatted;
        }
    }
}