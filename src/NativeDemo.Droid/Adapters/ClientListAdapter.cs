﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using NativeDemo.Common.Contracts;

namespace NativeDemo.Droid.Activities
{
    public class ClientListAdapter : BaseAdapter<Client>
    {
        private Activity _activity;
        private List<Client> _list;

        public ClientListAdapter(Activity activity, List<Client> list)
        {
            this._activity = activity;
            this._list = list;
        }

        public override Client this[int position] => this._list[position];

        public override int Count => this._list.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? this._activity.LayoutInflater.Inflate(Resource.Layout.ListRow, parent, false);
            var data = this[position];

            var textView1 = view.FindViewById<TextView>(Resource.Id.ListRow_TextView1);
            textView1.Text = data.Name;
            var textView2 = view.FindViewById<TextView>(Resource.Id.ListRow_TextView2);
            textView2.Text = data.ContactInfoFormatted;

            return view;
        }
    }
}