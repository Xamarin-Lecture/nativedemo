﻿using Android.App;
using Android.OS;
using Android.Widget;
using NativeDemo.Common;

namespace NativeDemo.Droid.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class ListActivity : BaseActivity
    {
        private ListView _listView;

        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState, Resource.Layout.ListActivity);

            var clients = await DemoService.Instance.GetClientList();

            this._listView = this.FindViewById<ListView>(Resource.Id.ListActivity_ListView);
            this._listView.Adapter = new ClientListAdapter(this, clients);
        }
    }
}