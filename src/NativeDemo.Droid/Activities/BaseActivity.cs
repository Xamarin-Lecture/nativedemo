﻿using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Newtonsoft.Json;

namespace NativeDemo.Droid.Activities
{
    public class BaseActivity : AppCompatActivity
    {
        private const string BundleExtraDataKey = "BundleExtraDataKey";

        protected virtual void OnCreate(Bundle savedInstanceState, int layoutId)
        {
            base.OnCreate(savedInstanceState);

            this.SetContentView(layoutId);

            this.RequestedOrientation = ScreenOrientation.Portrait;
            this.Window.SetFlags(WindowManagerFlags.Secure, WindowManagerFlags.Secure);
        }

        protected void NavigateTo<TActivity, TData>(TData data)
            where TActivity : BaseActivity
        {
            var intent = new Intent(this, typeof(TActivity));

            if (data != null)
            {
                intent.PutExtra(BaseActivity.BundleExtraDataKey, JsonConvert.SerializeObject(data));
            }

            this.StartActivity(intent);
        }

        protected TData GetData<TData>()
        {
            var serializedData = this.Intent.GetStringExtra(BaseActivity.BundleExtraDataKey);
            if (string.IsNullOrWhiteSpace(serializedData))
            {
                return default(TData);
            }

            return JsonConvert.DeserializeObject<TData>(serializedData);
        }
    }
}